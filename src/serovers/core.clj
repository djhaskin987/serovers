(ns serovers.core
  "Version comparison."
  (:require [clojure.string :as string]))
(def ^:private nullc (char 0))

#_(defmacro ^:no-doc dbg
    [body]
    `(let [x# ~body]
       (println "dbg:" '~body "=" x#)
       x#))

(defn- epoch
  [a]
  (let [results (re-find #"^(\p{Digit}+):(.*)$" a)]
    (if results
      [(java.lang.Integer/parseInt ^java.lang.Integer (get results 1))
       (get results 2)]
      [0 a])))

(defn-
  lexical-comparison
  "Lexically compare two characters according to debian version rules."
  [a b]
  (cond (= a b) 0
        (= a \~) -1
        (= b \~) 1
        (and (java.lang.Character/isLetter ^java.lang.Character a)
             (not (java.lang.Character/isLetter ^java.lang.Character b))
             (not (= b nullc)))
        -1
        (and (java.lang.Character/isLetter ^java.lang.Character b)
             (not (java.lang.Character/isLetter ^java.lang.Character a))
             (not (= a nullc)))
        1
        :else
        (- (int a) (int b))))

(defn- justify-strings
  "Returns two seqs of equal length, composed either
  of the characters from the strings, or the null character."
  [a b]
  (let [va (vec a)
        vb (vec b)
        ca (count a)
        cb (count b)]
    (cond
      (= ca
         cb)
      [va
       vb]
      (> cb ca)
      [(into va
             (repeat (- cb ca)
                     nullc))
       vb]
      :else
      [va
       (into vb
             (repeat (- ca cb)
                     nullc))])))

(defn debian-string-compare
  [a b]
  (or
   (some
    #(if (not (zero? %)) % nil)
    (let [[justa justb] (justify-strings a b)]
      (map
       lexical-comparison
       justa
       justb)))
   0))

(defn- numeric-part-compare
  "Compares two version parts, which both consist
  entirely of digits."
  [a b]
  (let [trimmed-a (string/replace a #"^0+" "")
        trimmed-b (string/replace b #"^0+" "")
        ldiff (- (count trimmed-a)
                 (count trimmed-b))]
    (if (zero? ldiff)
      (compare trimmed-a trimmed-b)
      ldiff)))

(defn epochless-debian-vercmp
  [a b]
  (if (and (empty? a)
           (empty? b))
    0
    (let [[_ initial-a] (re-find #"^([^\p{Digit}]*)" a)
          [_ initial-b] (re-find #"^([^\p{Digit}]*)" b)
          diff (debian-string-compare initial-a initial-b)]
      (if (zero? diff)
        (let [next-a (string/replace a #"^[^\p{Digit}]*" "")
              next-b (string/replace b #"^[^\p{Digit}]*" "")]
          (if (and (empty? next-a)
                   (empty? next-b))
            0
            (let [[_ numeric-a] (re-find #"^(\p{Digit}*)" next-a)
                  [_ numeric-b] (re-find #"^(\p{Digit}*)" next-b)
                  ndiff (numeric-part-compare numeric-a numeric-b)]
              (if (zero? ndiff)
                (recur (string/replace next-a #"^\p{Digit}*" "")
                       (string/replace next-b #"^\p{Digit}*" ""))
                ndiff))))
        diff))))

(defn debian-vercmp
  "Compares two debian version numbers according to the rules laid out in the
   [Debian Policy Manual](https://www.debian.org/doc/debian-policy/ch-controlfields.html#s-f-Version).

   Epoch numbers, upstream versions, and debian revision version parts are fully
   supported.

   All other vercmp algorithms in serovers are implemented in terms of this
   function."
  {:added "1.0"}
  [a b]
  (let [[a-epoch a-vers] (epoch a)
        [b-epoch b-vers] (epoch b)]
    (if (= a-epoch b-epoch)
      (epochless-debian-vercmp a-vers b-vers)
      (- a-epoch b-epoch))))

(defn maven-normalize
  "Convert a maven version number to a debian version number such that
   the function call `(debian-vercmp (maven-normalize a) (maven-normalize b))`
   would compare two correct maven version numbers correctly."
  {:added "1.1.0"}
  [vers]
  (as-> vers it
    (string/replace
     it
     #"[\p{Punct}&&[^~]]+([Aa][Ll][Pp][Hh][Aa]|[Aa])"
     "~~alpha")
    (string/replace
     it
     #"[\p{Punct}&&[^~]]+([Bb][Ee][Tt][Aa]|[Bb])"
     "~~beta")
    (string/replace
     it
     #"[\p{Punct}&&[^~]]+([Mm][Ii][Ll][Ee][Ss][Tt][Oo][Nn][Ee]|[Mm])"
     "~~milestone")
    (string/replace
     it
     #"[\p{Punct}&&[^~]]+([Rr][Cc]|[Cc][Rr])" "~~rc")
    (string/replace
     it
     #"[\p{Punct}&&[^~]]+([Ss][Nn][Aa][Pp][Ss][Hh][Oo][Tt])"
     "~~snapshot")
    (string/replace
     it
     #"[\p{Punct}&&[^~]]+([Gg][Aa])"
     "")
    (string/replace
     it
     #"[\p{Punct}&&[^~]]+([Ff][Ii][Nn][Aa][Ll])"
     "")
    (string/replace
     it
     #"[\p{Punct}&&[^~]]+([Ss][Tt][Aa][Bb][Ll][Ee])"
     "")
    (string/replace
     it
     #"[\p{Punct}&&[^~]]+(\p{Alpha}+)"
     "~$1")
    (string/replace
     it
     #"[\p{Punct}&&[^~]]+0+\b"
     "")))

(defn maven-vercmp
  "Compare two maven version numbers. Simply calls `maven-normalize` on each
   version and then returns the `debian-vercmp` of the results."
  {:added "1.1.0"}
  [a b]
  (debian-vercmp
   (maven-normalize a)
   (maven-normalize b)))

(defn rpm-normalize
  "Convert an rpm version number to a debian version number such that
   the function call `(debian-vercmp (rpm-normalize a) (rpm-normalize b))`
   would compare two correct rpm version numbers correctly."
  {:added "1.2.0"}
  [vers]
  (as-> vers it
    (string/replace
     it
     #"[\p{Punct}&&[^~]]+"
     ".")
    (string/replace
     it
     #"(\p{Alpha}+)\.(\p{Digit}+)"
     "$1$2")
    (string/replace
     it
     #"(\p{Digit}+)\.(\p{Alpha}+)"
     "$1$2")))

(defn rpm-vercmp
  "Compare two rpm version numbers. Simply calls `rpm-normalize` on each
   version and then returns the `debian-vercmp` of the results."
  {:added "1.2.0"}
  [a b]
  (debian-vercmp
   (rpm-normalize a)
   (rpm-normalize b)))

(defn semver-normalize
  "Convert a [SemVer](http://semver.org) version number to a debian version
   number such that the function call `(debian-vercmp (semver-normalize
  a) (semver-normalize b))` would compare two correct semver version numbers
  correctly."
  {:added "1.4.0"}
  [vers]
  (as-> vers it
    (string/replace
     it
     #"\+.*$"
     "")
    (string/replace
     it
     #"-"
     "~")))

(defn semver-vercmp
  "Compare two semver version numbers. Simply calls `semver-normalize` on each
   version and then returns the `debian-vercmp` of the results."
  {:added "1.4.0"}
  [a b]
  (debian-vercmp
   (semver-normalize a)
   (semver-normalize b)))

(defn rubygem-normalize
  "Convert a [Ruby Gem](https://rubygems.org/) version number to a
  debian version number such that the function call `(debian-vercmp
  (rubygem-normalize a) (rubygem-normalize b))` would compare two
  correct ruby gem version numbers correctly."
  {:added "1.5.0"}
  [vers]
  (as-> vers it
    (string/replace
     it
     #"(\p{Digit}+)(\p{Alpha}+)"
     "$1.$2")
    (string/replace
     it
     #"(\p{Alpha}+)(\p{Digit}+)"
     "$1.$2")
    (string/replace
     it
     #"(\p{Digit}+(\.\p{Digit}+)*)\.(\p{Alpha}.*)$"
     "$1~$3")))

(defn rubygem-vercmp
  "Compare two ruby gem version numbers. Simply calls
  `rubygem-normalize` on each version and then returns the
  `debian-vercmp` of the results."
  {:added "1.5.0"}
  [a b]
  (debian-vercmp
   (rubygem-normalize a)
   (rubygem-normalize b)))

(defn- python-local-part-normalize
  [part]
  (reduce
   (fn [c [f t]]
     (clojure.string/replace c f t))
   part
   (mapv #(do [%1 %2])
         "0123456789"
         "ABCDEFGHIJ")))

(defn python-normalize
  "Convert a python (pip) version number to a debian version number such that
  the function call `(debian-vercmp (python-normalize a) (python-normalize b))`
  would compare two correct python version numbers correctly according to the
  [PEP 440](https://www.python.org/dev/peps/pep-0440/) standard, *for version
  numbers which do not contain python local version parts*. These are handled
  specially in `python-vercmp`."
  {:added "1.6.0"}
  [vers]
  (as-> vers it
    (string/lower-case it)
    (string/replace
     it
     #"^(\p{Digit}+)!"
     "$1:")
        ; 1.0 1.0.post1 1.0.0
    (string/replace
     it
     #"\.post(\p{Digit}+)"
     "!~$1")
    (string/replace
     it
     #"(\p{Digit}+)(\p{Punct})?a(\p{Digit}+)"
     "$1~a$3")
    (string/replace
     it
     #"(\p{Digit}+)(\p{Punct})?b(\p{Digit}+)"
     "$1~b$3")
    (string/replace
     it
     #"(\p{Digit}+)(\p{Punct})?rc(\p{Digit}+)"
     "$1~rc$3")
    (string/replace
     it
     #"(\p{Digit}+)(\p{Punct})?c(\p{Digit}+)"
     "$1~rc$3")
    (string/replace
     it
     #"[.]dev(\p{Digit}+)"
     "~~dev$1")))

(defn- naive-partcmp
  "Compare two version parts of a naive version number."
  [a b]
  (cond (and (re-matches #"\p{Digit}+" a)
             (re-matches #"\p{Digit}+" b))
        (numeric-part-compare a b)
        (re-matches #"\p{Digit}+" a)
        1
        (re-matches #"\p{Digit}+" b)
        -1
        :else
        (compare a b)))

(defn naive-vercmp
  "Compare two version numbers, separated into parts by
  punctuation. Compare each part in turn. If both parts
  are numeric, comare them as numbers. If one part is
  numeric, but the other alphanumeric, the first part
  is newer. if both are alphanumeric, compare lexically.
  Continue until one of the parts is newer than the other,
  and return a -1, 0, or 1 in the usual manner to indicate
  if the second argument is newer, if they are both equal,
  or if the first argument is newer, respectively.
  This is, in fact, the old rpmvercmp algorithm. It is not
  used in the modern version of rpm; however, it is still
  used in comparing the local part of the python version
  comparison algorithm."
  {:added "1.6.0"}
  [a b]
  (let [a-parts (string/split a #"\p{Punct}+")
        b-parts (string/split b #"\p{Punct}+")]
    (if-let [result (first
                     (filter #(not (zero? %))
                             (mapv
                              naive-partcmp
                              a-parts
                              b-parts)))]
      result
      (- (count a-parts) (count b-parts)))))

(defn python-vercmp
  "Compare two python (pip) version numbers according to the [PEP
  440](https://www.python.org/dev/peps/pep-0440/) standard.

  First, it calls `python-normalize` on each version's public version
  identifier part and then returns the `debian-vercmp` of the results,
  *provided both version numbers do not contain a local version identifier*.

  If a local version id is present on one of the version numbers, but both
  version numbers are otherwise identical, the version with a local version
  identifier is considered newer. If both have local version ids, but they are
  otherwise equal, the local version ids are compared using the `naive-vercmp`
  function."
  {:added "1.6.0"}
  [a b]
  (let [a-pub (string/replace a #"\+.*$" "")
        b-pub (string/replace b #"\+.*$" "")
        pub-result
        (debian-vercmp
         (python-normalize a-pub)
         (python-normalize b-pub))]
    (if (zero? pub-result)
      (cond
        (and (string/includes? a "+")
             (string/includes? b "+"))
        (naive-vercmp
         (string/replace a #"^[^+]+\+" "")
         (string/replace b #"^[^+]+\+" ""))
        (string/includes? a "+")
        1
        (string/includes? b "+")
        -1
        :else
        0)
      pub-result)))
