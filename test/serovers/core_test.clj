(ns serovers.core-test
  (:require [clojure.test :refer :all]
            [serovers.core :refer :all]))

(deftest
  ^:version-comparison naive-cases
  (testing "Naive comparison cases"
    (are [v0 v1 r] (r ^java.lang.Integer (naive-vercmp v0 v1) 0)
      ""             ""              .equals
      "abc"          "abc"           .equals
      "a.b.c"        "a_b-c"         .equals
      "1.2.0"        "1.2"           >
      "1.a"          "1.2"           <
      "0100"         "100"           .equals
      "0100.al0100"  "100.al100"     <
      "1000.9.17"    "100.9.18"      >)))

; The following test case versions list is taken from the file
; "tests/test_version.py" of the `pypa/packaging` git repository, found here:
; https://github.com/pypa/packaging The top of that file carries this copyright
; notice:
;
; This file is dual licensed under the terms of the Apache License, Version
; 2.0, and the BSD License. See the LICENSE file in the root of this repository
; for complete details.
;
; The License associated with this list of version numbers can be found in
; the file `licenses/LICENSE.python_test` at the root of this repository.

(def python-versions [; Implicit epoch of 0
                      "1.0.dev456", "1.0a1", "1.0a2.dev456", "1.0a12.dev456", "1.0a12",
                      "1.0b1.dev456", "1.0b2", "1.0b2.post345.dev456", "1.0b2.post345",
                      "1.0b2-346", "1.0c1.dev456", "1.0c1", "1.0rc2", "1.0c3", "1.0",
                      "1.0.post456.dev34", "1.0.post456", "1.1.dev1", "1.2", "1.2+123abc",
                      "1.2+123abc456", "1.2+abc", "1.2+abc123", "1.2+abc123def", "1.2+1234.abc",
                      "1.2+123456", "1.2.r32+123456", "1.2.rev33+123456",

    ; Explicit epoch of 1
                      "1!1.0.dev456", "1!1.0a1", "1!1.0a2.dev456", "1!1.0a12.dev456", "1!1.0a12",
                      "1!1.0b1.dev456", "1!1.0b2", "1!1.0b2.post345.dev456", "1!1.0b2.post345",
                      "1!1.0b2-346", "1!1.0c1.dev456", "1!1.0c1", "1!1.0rc2", "1!1.0c3", "1!1.0",
                      "1!1.0.post456.dev34", "1!1.0.post456", "1!1.1.dev1", "1!1.2+123abc",
                      "1!1.2+123abc456", "1!1.2+abc", "1!1.2+abc123", "1!1.2+abc123def",
                      "1!1.2+1234.abc", "1!1.2+123456", "1!1.2.r32+123456", "1!1.2.rev33+123456"])

(defn test-python-pair [a b]
  (testing (str
            "python version `"
            a
            "` is less than `"
            b
            "`")
    (is (< (python-vercmp a b) 0))
    (is (> (python-vercmp b a) 0))
    (is (= (python-vercmp a a) 0))))

(deftest
  ^:version-comparison python-cases
  (testing "Python edge cases"
    (are [v0 v1 r] (r ^java.lang.Integer (python-vercmp v0 v1) 0)
      ""             ""              .equals
      "1.2.3rc1"     "1.2.3RC1"      .equals
      "1.0+foo0100"  "1.0+foo100"    <
      "1.0+0100foo"  "1.0+100foo"    <
      "1.0.a1"       "1.0a1"         .equals
      "1.0.rc1"      "1.0rc1"        .equals
      "1.0.b1"       "1.0b1"         .equals))
  (test-python-pair (first python-versions)
                    (peek python-versions))
  (doseq [[a b] (map
                 #(do [%1 %2])
                 (pop python-versions)
                 (rest python-versions))]
    (test-python-pair a b)))

; https://ruby-doc.org/stdlib-2.0.0/libdoc/rubygems/rdoc/Gem/Version.html
(deftest
  ^:version-comparison gem-cases
  (testing "Gem Cases"
    (are [v0 v1 r] (r ^java.lang.Integer (rubygem-vercmp v0 v1) 0)
      ""                ""              .equals
      "1.0.0"           "0.1.0"         >
      "1.0.0"           "2.0.0"         <
      "1.1.1"           "1.1.1"         .equals
      "2.0.0"           "2.1.0"         <
      "2.1.1"           "2.1.0"         >
      "1.0.1"           "1.0"           >
      "1.0.0"           "1.0.1"         <
      "1.0.0"           "0.9.2"         >
      "0.9.2"           "0.9.3"         <
      "0.9.2"           "0.9.1"         >
      "0.9.5"           "0.9.13"        <
      "10.2.0.3.0"      "11.2.0.3.0"    <
      "10.2.0.3.0"      "5.2.0.3.0"     >
      "1.0"             "1"             >
      "1.2.a"           "1.2"           <
      "1.2.z"           "1.2"           <
      "1.1.z"           "1.0"           >
      "1.0.a10"         "1.0.a9"        >
      "1.0"             "1.0.b1"        >
      "1.0.a2"          "1.0.b1"        <
      "1.0.a2"          "0.9"           >
      "1.0.a.10"        "1.0.a10"       .equals
      "1.0.a10"         "1.0.a.10"      .equals)))

(deftest
  ^:version-comparison semver-cases
  (testing "Semver Cases"
    (are [v0 v1 r] (r ^java.lang.Integer (semver-vercmp v0 v1) 0)
      ""            ""                      .equals
      "1.0.0+001"   "1.0.0+20130313144700"  .equals
      "1.0.0+exp.sha.5114f85" "1.0.0"       .equals
      "1.0.0-alpha" "1.0.0-alpha.1"         <
      "1.0.0-alpha.1" "1.0.0-alpha.beta"    <
      "1.0.0-alpha.beta" "1.0.0-beta"       <
      "1.0.0-beta.2" "1.0.0-beta"           >
      "1.0.0-beta.2" "1.0.0-beta.11"        <
      "1.0.0-beta.11" "1.0.0-rc.1"          <
      "1.0.0-rc.1"   "1.0.0"                <
      "1.0.0"        "0.1.0"                >
      "1.2.10"       "1.2.9"                >
      "9.8.7"        "9.8.7+burarum"        .equals
      "1.0.0"        "2.0.0"                <
      "2.0.0"        "2.1.0"                <
      "2.1.1"        "2.1.0"                >
      "1.0.0"        "1.0.0-alpha"          >)))

(deftest
  ^:version-comparison rpm-cases
  ; https://fedoraproject.org/wiki/Archive:Tools/RPM/VersionComparison
  (testing "Random cases"
    (are [v0 v1 r] (r ^java.lang.Integer (rpm-vercmp v0 v1) 0)
      ""            ""          .equals
      "1.2.3"       "1.2-3"     .equals
      "1.0010"      "1.9"       >
      "1.05"        "1.5"       .equals
      "1.0"         "1"         >
      "2.50"        "2.5"       >
      "fc4"         "fc.4"      .equals
      "FC5"         "fc4"       <
      "2a"          "2.0"       <
      "2.a"         "2a"        .equals
      "1.0"         "1.fc4"     >
      "3.0.0_fc"    "3.0.0.fc"  .equals
      "~~"          "~~a"       <
      "~"           "~~a"       >
      "~"           ""          <
      "a"           ""          >)))

(deftest
  ^:version-comparison debian-cases
  (testing "Debian Cases"
    (are [v0 v1 r] (r ^java.lang.Integer (debian-vercmp ^java.lang.String v0 ^java.lang.String v1) 0)
      ""            ""          .equals
      "~~"          "~~a"       <
      "~"           "~~a"       >
      "~"           ""          <
      "a"           ""          >
      "1.2.3~rc1"   "1.2.3"     <
      "1.2"         "1.2"       .equals
      "1.2"         "a1.2"      <
      "1.2.3"       "1.2-3"     >
      "1.2.3~rc1"   "1.2.3~~rc1"    >
      "1.2.3"       "2"         <
      "2.0.0"       "2.0"       >
      "1.2.a"       "1.2.3"     >
      "1.2.a"       "1.2a"      >
      "1"           "1.2.3.4"   <
      "1:2.3.4"     "1:2.3.4"   .equals
      "0:"          "0:"        .equals
      "0:"          ""          .equals
      "0:1.2"       "1.2"       .equals
      "0:"          "1:"        <
      "0:1.2.3"     "2:0.4.5"   <)))

(deftest maven-cases
  (are [v0 v1 r] (r ^java.lang.Integer (maven-vercmp v0 v1) 0)
              ;; Numeric Comparison
    "1.0.0"          "1.0.0"           .equals
    "1.0.0"          "1.0"             .equals
    "1.0.1"          "1.0"             >
    "1.0.0"          "1.0.1"           <
    "1.0.0"          "1.0.0-1"         <
    "1.0.0"          "0.9.2"           >
    "0.9.2"          "0.9.3"           <
    "0.9.2"          "0.9.1"           >
    "0.9.5"          "0.9.13"          <
    "10.2.0.3.0"     "11.2.0.3.0"      <
    "10.2.0.3.0"     "5.2.0.3.0"       >
    "1.0.0-SNAPSHOT" "1.0.1-SNAPSHOT"  <
    "1.0.0-alpha"    "1.0.1-beta"      <
    "1.1-dolphin"    "1.1.1-cobra"     <
              ;; Lexical Comparison
    "1.0-alpaca"     "1.0-bermuda"     <
    "1.0-alpaca"     "1.0-alpaci"      <
    "1.0-dolphin"    "1.0-cobra"       >

              ;; Qualifier Comparison
    "1.0.0-alpha"    "1.0.0-beta"      <
    "1.0.0-beta"     "1.0.0-alpha"     >
    "1.0.0-alpaca"   "1.0.0-beta"      <
    "1.0.0-final"    "1.0.0-milestone" >

              ;; Qualifier/Numeric Comparison
    "1.0.0-alpha1"   "1.0.0-alpha2"    <
    "1.0.0-alpha5"   "1.0.0-alpha23"   <
    "1.0-RC5"        "1.0-RC20"        <
    "1.0-RC11"       "1.0-RC6"         >


              ;; Releases are newer than SNAPSHOTs


    "1.0.0"          "1.0.0-SNAPSHOT"  >
    "1.0.0-SNAPSHOT" "1.0.0-SNAPSHOT"  .equals
    "1.0.0-SNAPSHOT" "1.0.0"           <


              ;; Releases are newer than qualified versions


    "1.0.0"          "1.0.0-alpha5"    >
    "1.0.0-alpha5"   "1.0.0"           <

              ;; SNAPSHOTS are newer than qualified versions
    "1.0.0-SNAPSHOT" "1.0.0-RC1"       >
    "1.0.0-SNAPSHOT" "1.0.1-RC1"       <

              ;; Some other Formats
    "9.1-901.jdbc4"   "9.1-901.jdbc3"   >
    "9.1-901-1.jdbc4" "9.1-901.jdbc4"   >

              ;; Some more zero-extension Tests
              ;;
    "1-SNAPSHOT"      "1.0-SNAPSHOT"    .equals
    "1-alpha"         "1-alpha0"        .equals))
