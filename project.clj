(defproject serovers "1.7.1-SNAPSHOT"
  :description "The last version comparison library you'll ever need"
  :url "http://gitlab.com/djhaskin987/serovers"
  :plugins [[lein-codox "0.10.3"]]
  :deploy-repositories [["clojars" {:url "https://clojars.org/repo"
                                    :username :env/CLOJARS_USERNAME
                                    :password :env/CLOJARS_PASSWORD
                                    :sign-releases false
                                    }]]
  :codox {:metadata {:doc/format :markdown}}
  :global-vars {*warn-on-reflection* true}
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.11.1"]])
