# Overview

Serovers was created to
support [degasolv](https://github.com/djhaskin987/degasolv) in its
package-manager world conquering. The key insight behind serovers is that, by
using the debian version comparison algorithm under the hood and simply
normalizing version numbers from other schemes so that they compare correctly,
(almost) all of the version comparison algorithms can be improved when I
profile and optimize just the debian version comparison algorithm. This
architecture also makes it really easy to add and maintain version comparison
algorithms.

One thing to note is that *normalize* here does not mean *bring
version into conformance*. A key feature of serovers is that it is
liberal in what it accepts. If you try to compare two versions using
`semver-vercmp` but one of them is not semver compliant, serovers will
do its best anyways, instead of failing on the spot. This is
important, because package version numbers are set by package
maintainers, and it would be bad to break a build over a technicality
when a package maintainer gets it wrong with the package version.
