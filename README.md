# serovers

[![Clojars Project](http://clojars.org/serovers/latest-version.svg)](http://clojars.org/serovers)

A Clojure library for version comparison. *Another* version comparison library, you ask? Well, yes.

Why? Because there are lots of version comparison schemes out there. This library 
supports several:

It *already* supports these standards:

  - rubygem
  - maven
  - debian
  - rpm
  - semver 2.0 (including ignoring build metadata)
  - python (via [PEP 440](https://www.python.org/dev/peps/pep-0440/))


It does this by normalizing the version number according what scheme
is being used, then feeds the newly transformed version numbers to the
venerable debian version comparison algorithm for comparison.

## Sero what now?
Named after [Valentin Serov](https://en.wikipedia.org/wiki/Valentin_Serov), the
painter.

## Usage

Just call

```
(serovers.core/<standard>-vercmp avers bvers)
```

and you'll get a negative, positive, or zero value depending on whether
`avers < bvers`, `avers > bvers`, or `avers = bvers`, respectively. :)

If this does not work as advertised, please log a bug :)

## License

Copyright © 2017 Daniel Jay Haskin et. al., see the AUTHORS.md file.

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
